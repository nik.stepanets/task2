using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Collections.Generic;
using System.Linq;

namespace task2
{
    public class Tests
    {
        private IWebDriver webDriver;

        [SetUp]
        public void Setup()
        {
            webDriver = new ChromeDriver();
            webDriver.Manage().Window.Maximize();
            webDriver.Navigate().GoToUrl("https://avic.ua");
        }

        [TearDown]
        public void Teardown()
        {
            webDriver.Quit();
        }

        [Test]
        public void TestSearchKeyword()
        {
            string keyword = "Nokia";

            IWebElement searchField = webDriver.FindElement(By.XPath("//input[@id='input_search']"));
            searchField.Clear();
            searchField.SendKeys(keyword);
            searchField.Submit();

            List<IWebElement> products = webDriver.FindElements(By.XPath("//div[contains(@class, 'item-prod')]")).ToList();

            foreach (IWebElement product in products)
            {
                string productText = product.FindElement(By.XPath(".//div[@class='prod-cart__descr']")).Text;
                Assert.That(productText, Does.Contain(keyword));
            }

        }

        [Test]
        public void TestSearchKeywords()
        {
            string keyword1 = "Dell";
            string keyword2 = "HP";

            IWebElement searchField = webDriver.FindElement(By.XPath("//input[@id='input_search']"));
            searchField.Clear();
            searchField.SendKeys($"{keyword1} {keyword2}");
            searchField.Submit();

            List<IWebElement> products = webDriver.FindElements(By.XPath("//div[contains(@class, 'item-prod')]")).ToList();

            foreach (IWebElement product in products)
            {
                string productText = product.FindElement(By.XPath(".//div[@class='prod-cart__descr']")).Text;
                Assert.That(productText.Contains(keyword1) || productText.Contains(keyword2), Is.True);
            }

        }

        [Test]
        public void TestDefaultSorting()
        {
            webDriver.FindElement(By.XPath("//span[text()='Смартфони та телефони']")).Click();
            webDriver.FindElement(By.XPath("//a[text()='Смартфони'][not(@class)]")).Click();
            IWebElement sortOrderDropdown = webDriver
                .FindElement(By.XPath("//div[@class='two-column-wrapper ']//span[@role='combobox']"));
            string selectedSortOrder = sortOrderDropdown.FindElement(By.XPath("./span[@id]")).Text;
            Assert.That(selectedSortOrder, Is.EqualTo("За популярністю"));

        }
    }
}